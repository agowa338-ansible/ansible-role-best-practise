# Project Name Here
Enter a short description of what this role is capable of. 
This Role is just an example for showing how to write a good ansible role and how it should be structured.

# License
For license information consult the file LICENSE in the root of this project.

# Other stuff
Generate a new role directory:
```
ansible-galaxy init "Role-Name"
```
https://www.codereviewvideos.com/course/ansible-tutorial/video/variable-precedence-where-to-put-your-role-vars

# Files and Folders
(This is not required for an actual role, but because this is an example)
For an ansible role you don't need to include all of the following folders, but depending on what you want do do with your role you should consider adding them. Often these folders are added despite beeing empty (using an empty ".gitkeep" file within).

```
.
├── .README.md.kate-swp
├── .gitignore
├── LICENSE
├── README.md
├── defaults
│   └── main.yml
├── files
│   └── example.file
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── tasks
│   └── main.yml
├── templates
│   └── example.cfg.j2
└── vars
    ├── archlinux.yml
    ├── debian.yml
    └── main.yml
```

## Where to put variables?
Should variables be put into the "defaults" or "vars" directory?
Well it depends. 
If you want to specify variables that should not be overwritten like platform specific stuff, you would use the vars directory. For example you want to design your role to support debian as well as archlinux. But there lets say the list of dependencies differ. In this case you would put the list of packages into the cars file and import it if the role is ran on that specific platform. The variables in vars can only be overwritten by specifying them on the commandline. 
If you want to provide some configuration options for your users, you should use the defaults directory, as thouse values can be overwritten by variables within the playbook specified by the user of your role.

## Where to put binary files?
Into the files directory

## Where to put private keys?
Not into the role. These should be (if really required) only be stored within the playbook, but only encrypted using ansible-vault or git-crypt.

## What are templates?
Templates are generalized configuration files, you use jinja2 to "script your configuration file".

## What is this meta folder?
The meta folder contains methadata not required by ansible it self, but by ansible-galaxy. It is highly recommended to use ansible-galaxy for your playbook. 
Ansible Galaxy allows you to put each role into a separate repository and pull them at runtime. It is similar to git submodules, but with the benefit of not requireing a git pull within the submodule to fetch newer versions of that module. You only need to specify which branch (or tag) to pull when executing the role from a playbook.

### So what's inside of the meta folder?
Inside the meta folder there is suprisingly a main.yml file. 
So that may not have been the answer you've been looking for, but now you may ask "but what's inside of that main.yml?

```
galaxy_info:
  author: YOUR-NAME
  company: YOUR COMPANY, if not aplicable (for individuals), just remove this line.
  description: SHORT DESCRIPTION OF YOUR ROLE
  min_ansible_version: 2.1
  license: MIT
  platforms:
    - name: Archlinux
      versions:
        - all
    - name: Debian
      versions:
        - all
    - name: Ubuntu
      versions:
        - all
  categories:
    - documentation
dependencies: []
```
